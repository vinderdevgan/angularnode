
## Steps to run server

1.  Download [Docker](https://www.docker.com/get-started)
2.  Inside source folder run the following commands in cmd prompt/terminal:
    1.  ```docker build ./```
    2.  ```docker images```
    3.  pick the latest IMAGE ID
	4.	```docker run -d -p 3001:3001 {paste image-id here}```

###### done! server is connected 

```diff
- caution! connected to main cloud database
```