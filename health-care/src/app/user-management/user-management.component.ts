import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog'
import { DialogComponent } from '../dialog/dialog.component';
import { AuthService } from '../auth/auth.service';
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../model/app.model';
import { PageEvent } from '@angular/material/paginator';
import _ from 'lodash';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit, OnDestroy {
  private userListSub: Subscription
  public userlist: User[] = []
  roles = []
  searchListener = new Subject<string>();

  public userPagination = {
    search: '',
    length: 0,
    page: 0,
    pagesize: 10
  }
  public displayColums = ['username', 'email', 'name', 'active', 'group', 'actions']
  constructor(private authService: AuthService,
    private dialog: MatDialog) {
    this.searchListener.pipe(
      debounceTime(400),
      distinctUntilChanged())
      .subscribe(value => {
        this.authService.userlist(this.userPagination)
      });
  }

  async ngOnInit() {
    this.authService.userlist(this.userPagination)
    this.roles = await this.authService.fetchRoles()
    this.userListSub = this.authService.getUserListListener()
      .subscribe((result: { users: User[], usersCount: Number }) => {

        const sortedData = _.orderBy(result.users, [r => r.username], ['asce']);
        this.userlist = sortedData
        this.userPagination.length = +result.usersCount
      })
  }
  ngOnDestroy() {
    this.userListSub.unsubscribe()
  }
  getRole(role) {
    return this.roles[this.roles.findIndex(obj => obj._id === role)].role
  }

  onCreate() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    dialogConfig.width = '60%'
    this.dialog.open(DialogComponent, dialogConfig)
  }
  onUpdate(row: any) {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    dialogConfig.width = '60%'
    dialogConfig.data = row
    this.dialog.open(DialogComponent, dialogConfig)
  }
  onDelete(username: string) {
    this.authService.onUserListDelete(username)
  }
  onChangedPage(pageEvent: PageEvent) {
    this.userPagination.page = pageEvent.pageIndex
    this.userPagination.pagesize = pageEvent.pageSize
    this.authService.userlist(this.userPagination)
  }

}
