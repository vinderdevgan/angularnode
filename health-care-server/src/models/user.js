const mongoose = require("mongoose");
const Schema = mongoose.Schema
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
  _id: { type: Schema.Types.ObjectId, required: true },
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  active: { type: Boolean, required: true, default: true },
  role: { type: Schema.Types.ObjectId, required: true, ref: 'Role' }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
