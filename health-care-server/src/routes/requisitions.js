const express = require("express");
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
const jwt = require("jsonwebtoken");
var ObjectId = require('mongodb').ObjectID;
const { config, hash, compare, token } = require('./../config')
const { auth } = require("../middleware/check-auth");
const Requisition = require("../models/requisitions");
const Test = require("../models/tests");
const { generatePdf } = require("../../utils/pdfGenerator")
const _ = require("lodash");
const PDFMerge = require('pdf-merge');

const router = express.Router();


router.post("/", async (req, res) => {
  let randomNumber = Math.floor(Math.random() * 1000000000);
  const findRequisition = (accession) => Requisition.find({ accession: accession }).then((res) => {
    if (res.length > 0) {
      randomNumber = Math.floor(Math.random() * 1000000000)
      findRequisition(randomNumber)
    }
  })
  findRequisition(randomNumber)
  const requisition = new Requisition({
    _id: new ObjectId(),
    phn: req.body.phn,
    accession: randomNumber,
    description: req.body.description,
    lastName: req.body.lastName,
    firstName: req.body.firstName,
    physician: req.body.physician,
    physicianCode: req.body.physicianCode,
    sex: req.body.sex,
    dob: req.body.dob,
    postalCode: req.body.postalCode,
    tests: req.body.tests,
    toCollect: req.body.collect === 'Y', // remove this and implement a proper logic -- idiot -- vinder
    hidden: req.body.hidden,
    mode: req.body.mode,
    collectionDate: req.body.collectionDate,
    collectionTime: req.body.collectionTime,
    collectionLocation: req.body.collectionLocation
  })

  requisition.save()
    .then((data) => {
      res.status(200).json({
        message: 'Created',
        data: data
      })
    })
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    })
})

router.put("/", async (req, res) => {
  const { requisition, _id, ...body } = req.body;
  Requisition.findOneAndUpdate({
    accession: body.accession
  }, {
    $set: body
  }, {
    upsert: true
  }, function (err) {
    if (err) {
      console.log(err)
    } else {
      res.status(200).json({
        message: 'Updated'
      })

    }
  });
});

router.get("/", (req, res, next) => {
  Requisition.find()
    .then(documents => {
      res.status(200).json({
        requisitions: documents
      });
    });
});

router.get("/random-reqiuisition", async (req, res) => {
  let filteredRequisition
  let response
  Requisition.find({ mode: req.query.mode, hidden: false })
    .then(async (requisitions) => {
      if (requisitions.length > 0) {
        const index = _.random(0, requisitions.length - 1)
        const testcode = []
        filteredRequisition = JSON.parse(JSON.stringify(requisitions[index]))
        const testResponse = await Test.find();
        filteredRequisition.tests.map((testId) => {
          const testData = testResponse.filter((t) => t._id == testId)
          if (testData.length > 0) {
            testcode.push(testData[0].code)
          }
        })
        filteredRequisition.tests = testcode;
        response = await generatePdf(filteredRequisition)
      } else {
        throw new Error('Error generating pdf');
      }
      res.status(200).json({
        message: 'pdf generated',
        requisition: filteredRequisition,
        url: response.url
      });
    }).catch((err) => {
      res.status(400).json({
        message: err
      });
    });
});

router.get("/multiple-random-reqiuisition", async (req, res) => {
  let response
  let ress = []
  Requisition.find({ mode: 'practice', hidden: false })
    .then(async (requisitions) => {
      if (requisitions.length >= 10) {
        const shuffledRequisition = _.shuffle(requisitions)
        for (let i = 0; i < 10; i++) {
          const testcode = []
          const temp = JSON.parse(JSON.stringify(shuffledRequisition[i]))
          const testResponse = await Test.find();
          const tests = temp.tests
          delete temp.tests;
          tests.map((testId) => {
            const testData = testResponse.filter((t) => t._id == testId)
            if (testData.length > 0) {
              testcode.push(testData[0].code)
            }
          })
          temp.tests = testcode;
          response = await generatePdf(temp)
          response.url && ress.push(response.url)
        }
      }
      else if (requisitions.length > 0) {
        const shuffledRequisition = _.shuffle(requisitions)
        for (let i = 0; i < shuffledRequisition.length; i++) {
          const testcode = []
          const temp = JSON.parse(JSON.stringify(shuffledRequisition[i]))
          const testResponse = await Test.find();
          const tests = temp.tests
          delete temp.tests;
          tests.map((testId) => {
            const testData = testResponse.filter((t) => t._id == testId)
            if (testData.length > 0) {
              testcode.push(testData[0].code)
            }
          })
          temp.tests = testcode;
          response = await generatePdf(temp)
          response.url && ress.push(response.url)
        }
      }
      else {
        throw new Error('Error generating pdf');
      }

      const files = []

      ress.map((fileName) => {
        files.push(`./public/${fileName}`)
      })

      //Buffer (Default)
      PDFMerge(files)
      //Stream
      PDFMerge(files, { output: 'Stream' })
      //Save as new file
      PDFMerge(files, { output: `./public/${ress[0]}` })
        .then((buffer) => {
          res.status(200).json({
            message: 'pdf generated',
            url: `./${ress[0]}`
          });
        });
    }).catch((err) => {
      res.status(400).json({
        message: err
      });
    });
});

router.delete('/:id', async (req, res, next) => {
  Requisition.findByIdAndDelete({ _id: req.params.id })
    .then(() => {
      res.status(200).json({ message: `Deleted  requisition ${req.params.id} succesfully` });
    })
    .catch(error => {
      res.status(500).json({
        message: error
      });
    });
})

module.exports = router;
