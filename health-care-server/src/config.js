
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const setup = {}
const db = {
    name: 'healthCare',
    password: 'mongodb00'
}
setup.config = {
    secretKey: 'secret_this_should_be_longer',
    expiresIn: '1h'
}
setup.connectDB = `mongodb://meandb:${db.password}@cluster0-shard-00-00-qrobu.mongodb.net:27017,cluster0-shard-00-01-qrobu.mongodb.net:27017,cluster0-shard-00-02-qrobu.mongodb.net:27017/${db.name}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority`;

setup.hash = (password) => bcrypt.hash(password, 10)
setup.compare = (password, password2) => bcrypt.compare(password, password2)
setup.verify = (token, key) => jwt.verify(token, key);
setup.token = (_id, username, email) => jwt.sign({ _id: _id, username: username, email: email }, setup.config.secretKey, { expiresIn: setup.config.expiresIn })
module.exports = setup  