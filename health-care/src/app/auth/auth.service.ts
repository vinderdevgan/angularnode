import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Login, User } from '../model/app.model';
import api from '../../api';

@Injectable({ providedIn: "root" })
export class AuthService {

    private isAuthenticated = false;
    private token: string;
    private isAdmin: boolean;
    private isAdminListner = new Subject<boolean>();
    private tokenTimer: any;
    private users: User[] = []
    private pagination = {
        search: '',
        length: 0,
        page: 0,
        pagesize: 10
    }
    private testList: any
    roles: any = []
    private authStatusListener = new Subject<boolean>();
    private requisitionListListener = new Subject<boolean>();
    private userListListener = new Subject<{ users: User[], usersCount: Number }>();
    private testListListener = new Subject<any>();
    // private userListListener = new Subject<[any]>();

    constructor(private http: HttpClient, private router: Router) { }

    login(username: String, password: String) {
        const data: Login = { username: username, password: password }
        this.http.post<any>(api('api/user/login'), data)
            .subscribe(response => {
                this.token = response.token
                if (this.token) {
                    // this.tokenTimer = setTimeout(() => this.logout(), 1000 * response.expiresIn)
                    this.setAuthTimer(response.expiresIn)
                    this.isAuthenticated = true
                    this.authStatusListener.next(true)
                    this.isAdminListner.next(response.isAdmin)
                    this.isAdmin = response.isAdmin
                    const now = new Date();
                    const expirationDate = new Date(now.getTime() + response.expiresIn * 1000);
                    this.saveAuthData(this.token, expirationDate, this.isAdmin);
                    this.router.navigate(['/DataEntryForm']);
                }
            })
    }


    logout() {
        this.token = null;
        this.isAuthenticated = false;
        this.authStatusListener.next(false);
        this.isAdminListner.next(false);
        clearTimeout(this.tokenTimer);
        this.clearAuthData();
        this.router.navigate(['/']);
    }

    getToken() {
        return this.token;
    }

    getIsAuth() {
        return this.isAuthenticated;
    }

    getIsAdmin() {
        return this.isAdmin;
    }

    getIsAdminListener() {
        return this.isAdminListner.asObservable();
    }

    getAuthStatusListener() {
        return this.authStatusListener.asObservable();
    }
    getUserListListener() {
        return this.userListListener.asObservable();
    }
    getTestListListener() {
        return this.testListListener.asObservable();
    }
    getRequisitionListListener() {
        return this.requisitionListListener.asObservable();
    }

    autoAuthUser() {
        const authInformation = this.getAuthData();
        if (!authInformation) {
            return;
        }
        const now = new Date();
        const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
        if (expiresIn > 0) {
            this.token = authInformation.token;
            this.isAdmin = authInformation.isAdmin == 'true' ? true : false;
            this.isAuthenticated = true;
            this.setAuthTimer(expiresIn / 1000);
            this.authStatusListener.next(true);
            this.isAdminListner.next(this.isAdmin)
        }
    }

    private setAuthTimer(duration: number) {
        this.tokenTimer = setTimeout(() => {
            this.logout();
        }, duration * 1000);
    }

    private saveAuthData(token: string, expirationDate: Date, isAdmin: boolean = false) {
        localStorage.setItem('token', token);
        localStorage.setItem('isAdmin', String(isAdmin));
        localStorage.setItem('expiration', expirationDate.toISOString());
    }

    private clearAuthData() {
        localStorage.removeItem('token');
        localStorage.removeItem('isAdmin');
        localStorage.removeItem('expiration');
        localStorage.removeItem('practiceRequisitionList');
        localStorage.removeItem('testRequisitionList');
        localStorage.removeItem('dataInputs');
    }

    private getAuthData() {
        const token = localStorage.getItem('token');
        const expirationDate = localStorage.getItem('expiration');
        const isAdmin = localStorage.getItem('isAdmin');
        if (!token || !expirationDate) {
            return;
        }
        return {
            token,
            expirationDate: new Date(expirationDate),
            isAdmin: isAdmin
        };
    }

    newUser(user: any) {
        this.http.post<any>(api('api/user/new-user'), user)
            .subscribe(result => {
                this.users.push(result.user)
                this.pagination.length = +this.pagination.length + 1
                this.userListListener.next({
                    users: [...this.users],
                    usersCount: this.pagination.length
                })
            })
    }

    saveRequisition(requisition: any): Promise<any> {
        const call = requisition.accession ? this.http.put<any>(api('api/requisition'), requisition) : this.http.post<any>(api('api/requisition'), requisition)
        return new Promise<any[]>(resolve =>
            call
                .subscribe(async (res) => {
                    await this.getRequisition();
                    resolve(res)
                })
        )
    }



    getRequisition(): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.get<any>(api('api/requisition'))
                .subscribe((res) => {
                    this.requisitionListListener.next(res.requisitions)
                    resolve(res)
                })
        )
    }

    removeRequisition(id): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.delete<any>(api('api/requisition/') + id)
                .subscribe(async (res) => {
                    await this.getRequisition();
                    resolve(res)
                })
        )
    }


    fetchRoles(): Promise<any> {
        return new Promise<any[]>(resolve => {
            if (this.roles.length) {
                resolve(this.roles)
            } else {
                this.http.get<any>(api('api/role'))
                    .subscribe(async (res) => {
                        this.roles = res.roles
                        resolve(this.roles)
                    })
            }
        })
    }

    getRoles() {
        return this.roles
    }

    getTestslist() {
        this.http.get<any>(api('api/test'))
            .subscribe(res => {
                this.testListListener.next(res.tests)
                this.testList = res.tests
            })
    }

    getTestListJson() {
        return this.testList
    }

    generatePdf(payload: any): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.post<any>(api('api/pdf'), payload)
                .subscribe((res) => {
                    resolve(res)
                })
        )
    }

    printPdf(payload: any): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.post<any>(api('api/pdf/print'), payload)
                .subscribe((res) => {
                    resolve(res)
                })
        )
    }

    generatemultipleRandomPdf(): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.get<any>(api('api/requisition/multiple-random-reqiuisition'))
                .subscribe((res) => {
                    resolve(res)
                })
        )
    }

    generateRandomPdf(mode: any): Promise<any> {
        return new Promise<any[]>(resolve =>
            this.http.get<any>(api('api/requisition/random-reqiuisition'), { params: { mode: mode } })
                .subscribe((res) => {
                    resolve(res)
                })
        )
    }

    userlist(userPagination: any) {
        this.pagination = userPagination
        const queryParam = `?search=${this.pagination.search}&pagesize=${this.pagination.pagesize}&page=${this.pagination.page}`
        this.http.get<{ status: Boolean, code: Number, userlist: [User], count: Number }>(api('api/user/all-user') + queryParam)
            .subscribe(result => {
                if (result.status) {
                    this.users = result.userlist
                    this.pagination.length = +result.count
                    this.userListListener.next({
                        users: [...this.users],
                        usersCount: this.pagination.length
                    })
                }
            })
    }
    onUserListDelete(username: string) {
        this.http.delete(api('api/user/') + username)
            .subscribe((result: { message: String }) => {
                this.users = this.users.filter(user => user.username !== username)
                this.pagination.length = +this.pagination.length - 1
                this.userListListener.next({
                    users: [...this.users],
                    usersCount: this.pagination.length
                })
            })
    }
    onUserListUpdate(user: User) {
        this.http.put<any>(api('api/user/update-user'), user)
            .subscribe(result => {
                let i = this.users.findIndex(user => user._id === result.user._id)
                if (i > -1) {
                    this.users[i] = result.user
                    this.userListListener.next({
                        users: [...this.users],
                        usersCount: this.pagination.length
                    })
                }
            })
    }
}
