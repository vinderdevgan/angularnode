import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import api from '../../api'

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  private authListnerSubs: Subscription
  isLoading = false;
  isAdmin: any = false;
  userIsAuthenticated = false;
  constructor(private authService: AuthService) { }
  ngOnInit(): void {
    this.isAdmin = this.authService.getIsAdmin()
    this.authService.getIsAdminListener()
      .subscribe(res => this.isAdmin = res)
    this.userIsAuthenticated = this.authService.getIsAuth()
    this.authListnerSubs = this.authService.getAuthStatusListener()
      .subscribe(bol => this.userIsAuthenticated = bol)
  }
  ngOnDestroy(): void {
    this.authListnerSubs.unsubscribe()
  }
  onLogout() {
    this.authService.logout()
  }
  async multiplePdf() {
    this.isLoading = true
    const res = await this.authService.generatemultipleRandomPdf()
    this.isLoading = false
    var link = document.createElement('a');
    link.setAttribute('download', null);
    link.setAttribute('target', '_blank');
    link.style.display = 'none';
    document.body.appendChild(link);
    link.setAttribute('href', `${api(res.url)}`);
    link.click();
    document.body.removeChild(link);
  }
}
