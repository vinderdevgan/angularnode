const express = require("express");
const router = express.Router();
const { generatePdf } = require("../../utils/pdfGenerator")

router.post("/", async (req, res, next) => {
  let status;
  let code;
  let response
  try {
    status = true;
    code = 200;
    response = await generatePdf(req.body);
  } catch (error) {
    status = false;
    code = 400;
  }

  res.status(code).json({
    status: 'status',
    code: code,
    url: response && response.url
  })
})

module.exports = router;
