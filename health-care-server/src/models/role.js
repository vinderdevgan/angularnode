const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const roleSchema = mongoose.Schema({
  _id: { type: String, required: true },
  role: { type: String, required: true, unique: true, lowercase: true, trim: true }
});
roleSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Role", roleSchema);
