export interface User {
    _id: String,
    username: String,
    email: String,
    password: String,
    name: String,
    active: Boolean,
    role: String
}
export interface Login {
    username: String,
    password: String
}