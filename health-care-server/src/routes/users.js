const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose')
const { config, hash, compare, token } = require('./../config')
const { auth } = require("../middleware/check-auth");
const User = require("../models/user");
const Role = require('../models/role');
const router = express.Router();

router.post("/new-user", auth, async (req, res, next) => {
  const user = new User({
    _id: null,
    username: req.body.username,
    email: req.body.email,
    password: await hash(req.body.password),
    name: req.body.name,
    active: req.body.active,
    role: req.body.role
  })
  user.save()
    .then(result => {
      let user = {
        _id: result._id,
        username: result.username,
        email: result.email,
        name: result.email,
        active: result.active
      }

      res.status(200).json({
        message: 'user created',
        user: user
      })
    })
    .catch(error => {
      res.status(500).json({
        message: 'user exist'
      })
    })
})
router.put("/update-user", auth, async (req, res, next) => {
  const _id = mongoose.Types.ObjectId(req.body._id)
  const user = {
    username: req.body.username,
    email: req.body.email,
    password: await hash(req.body.password),
    name: req.body.name,
    active: req.body.active,
    role: req.body.role
  }
  User.findByIdAndUpdate({ _id }, user, { new: true }).select("-password")
    .then(result => {
      res.status(200).json({
        message: 'user updated',
        user: result
      })
    }).catch(error => {
      res.status(500).json({
        message: 'user update failed',
        error: error
      })
    })

})

router.get("/all-user", auth, (req, res, next) => {
  const query = {
    $or: [
      { username: { $regex: '.*' + req.query.search + '.*', $options: 'i' } },
      { email: { $regex: '.*' + req.query.search + '.*', $options: 'i' } },
      { name: { $regex: '.*' + req.query.search + '.*', $options: 'i' } }
    ]
  }
  const pageSize = +req.query.pagesize;
  const page = +req.query.page;
  User.find(query)
    .select("-password")
    .skip(pageSize * (page))
    .limit(pageSize)
    .then(async (result) => {
      res.status(200).json({
        status: true,
        code: 200,
        userlist: result,
        count: await User.countDocuments(query)
      })
    })
})
router.post("/signup", (req, res, next) => {

  bcrypt.hash(req.body.password, 10).then(hash => {
    const user = new User({
      _id: null,
      username: req.body.username,
      email: req.body.email,
      password: hash,
      name: req.body.name,
      active: req.body.active,
      role: req.body.role
    });
    user
      .save()
      .then(() => {
        res.status(201).json({
          message: "User created!"
        });
      })
      .catch(err => {
        res.status(500).json({
          message: "User already Exists"
        });
      });
  });
});

router.post("/login", (req, res, next) => {
  let mUser;
  let isAdmin = false;
  User.findOne({ username: req.body.username })
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "Auth failed",
          token: null,
          expiresIn: null
        });
      }
      mUser = user;
      return compare(req.body.password, user.password)
    })
    .then(result => {
      if (!result) {
        return res.status(401).json({
          message: "Auth failed",
          token: null,
          expiresIn: null
        });
      }
      Role.find()
        .then((res) => {
          const userRole = res.filter((role) => String(role._id) === String(mUser.role))
          if (userRole.length > 0) {
            isAdmin = (String(userRole[0].role) === 'admin' || String(userRole[0].role) === 'instructor') ? true : false
          }
        })
        .then(() => {
          res.status(200).json({
            type: 'fetchedUser.type',
            token: token(mUser._id, mUser.username, mUser.email),
            expiresIn: 3600,
            isAdmin: isAdmin
          });
        })

    })
    .catch(err => {
      return res.status(401).json({
        message: "Auth failed"
      });
    });
});

router.delete("/:username", auth, (req, res) => {
  User.deleteOne({ username: req.params.username })
    .then(result => {
      if (result.n > 0) {
        res.status(200).json({
          status: true,
          code: 200,
          message: "Deletion successful!"
        });
      } else {
        res.status(401).json({
          status: false,
          code: 401,
          message: "Not authorized!"
        });
      }
    })
    .catch(error => {
      res.status(500).json({
        message: "Deleting posts failed!"
      });
    });
})

module.exports = router;

