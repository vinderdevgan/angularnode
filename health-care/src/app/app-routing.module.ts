import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { LoginCheck } from './auth/auth.login';
import { UserManagementComponent } from './user-management/user-management.component';
import { EntryFormComponent } from './entry-form/entry-form.component';
import { RequisitionComponent } from './requisition/requisition.component';
import { PrintComponent } from './print/print.component';

const routes: Routes = [
  { path: "", component: LoginComponent, canActivate: [LoginCheck] },
  { path: "UserManagement", component: UserManagementComponent, canActivate: [AuthGuard] },
  { path: "DataEntryForm", component: EntryFormComponent, canActivate: [AuthGuard] },
  { path: "Requisition", component: RequisitionComponent, canActivate: [AuthGuard] },
  { path: "Print", component: PrintComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, LoginCheck]
})
export class AppRoutingModule { }
