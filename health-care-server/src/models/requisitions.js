const mongoose = require("mongoose");
const Schema = mongoose.Schema

const requisitionSchema = mongoose.Schema({
  _id: { type: Schema.Types.ObjectId, required: true },
  phn: { type: Number, required: true, min: 100000000, max: 999999999, unique: true },
  accession: { type: Number, required: true, min: 100000000, max: 999999999, unique: true },
  description: { type: String },
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  sex: { type: String, enum: ['M', 'F'], default: 'M' },
  dob: { type: Date, required: true },
  postalCode: { type: String, required: true },
  toCollect: { type: Boolean, default: false },
  physician: { type: String, required: true },
  physicianCode: { type: String, required: true },
  collectionDate: { type: Date },
  collectionTime: { type: String },
  collectionLocation: { type: String },
  tests: [{ type: Schema.Types.ObjectId, ref: 'Test', default: [] }],
  hidden: { type: Boolean, required: true, default: false },
  mode: { type: String, enum: ['practice', 'test'], required: true, default: 'practice' }
});

module.exports = mongoose.model("Requisition", requisitionSchema);
