import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import * as JsBarcode from 'JsBarcode';
import PHE from 'print-html-element'
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { TooltipPosition } from '@angular/material/tooltip';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.css']
})
export class PrintComponent implements OnInit {
  positionOptions: TooltipPosition[] = ['below', 'above', 'left', 'right'];
  position = new FormControl(this.positionOptions[0]);
  selectItem = []
  selectedPrintOptions = {
    Requisition: false,
    Barcode: false,
    Label: false
  }
  printingOptions = [{ id: 'Requisition', value: 'Patient Record' }, { id: 'Barcode', value: 'Barcode Label' }, { id: 'Label', value: 'Collection List' }]
  requisitionList = [];
  requisitionObj = {};
  testList = [];
  labelsObject = {};

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.getRequisition();
    this.authService.getTestslist();
    this.authService.getRequisitionListListener()
      .subscribe((res: any) => {
        res.map((requisition) => {
          this.requisitionObj[requisition._id] = requisition
        })
        this.authService.getTestListListener()
          .subscribe((testRes: any) => {
            this.testList = testRes;
            res.map((r) => {
              if (r.collectionDate) {
                r.collectionDate = moment(r.collectionDate).format('MM-DD-YYYY')
              }
              if (r.dob) {
                r.dob = moment(r.dob).format('MM-DD-YYYY')
              }
            })
            this.requisitionList = res;
          })
      })
  }

  async pdf() {
    let dataArr: any = []
    setTimeout(() => {
      let height = 0
      this.selectedPrintOptions.Requisition && dataArr.push(document.getElementsByClassName("data-requisition"))
      this.selectedPrintOptions.Label && dataArr.push(document.getElementsByClassName("data-label"))
      this.selectedPrintOptions.Barcode && dataArr.push(document.getElementsByClassName("data-barcode"))
      for (let i = 0; i < dataArr.length; i++) {
        for (let x = 0; x < dataArr[i].length; x++) {
          const elementHeight = dataArr[i][x].offsetHeight;
          height = height + elementHeight
          if (height > 1000) {
            height = elementHeight;
            dataArr[i][x].style.pageBreakAfter = "always"
          }
        }
      }
    }, 0)
    setTimeout(() => {
      PHE.printElement(document.getElementById('page'))
    }, 10)
  }

  onSelectOptions(event) {
    if (this.selectedPrintOptions[event.source.value]) {
      this.selectedPrintOptions[event.source.value] = false
    } else {
      this.selectedPrintOptions[event.source.value] = true
    }
  }

  onSelect(event) {
    if (event.checked) {
      this.selectItem.push(event.source.value)
    } else {
      this.selectItem = this.selectItem.filter(item => item != event.source.value)
    }
    this.selectItem.map((requisitionId) => {
      let testLabel = []
      const requisition = this.requisitionList.filter((r) => r._id === requisitionId)[0]
      requisition.tests.map((test) => {
        testLabel.push(this.testList.filter((t) => t._id === test)[0])
      })
      this.labelsObject[requisition._id] = testLabel
      const id = '#x'.concat(requisitionId)
      setTimeout(() => {
        if (document.getElementById('x'.concat(requisitionId))) {
          JsBarcode(String(id), String(this.requisitionObj[requisitionId].phn), {
            format: "code128",
            height: 30,
            width: 1.5,
            background: 'rgba(255, 69, 0, 0)',
            displayValue: false
          })
        }
      }, 0)
    })
  }
}
