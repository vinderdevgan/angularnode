import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserManagementComponent } from '../user-management/user-management.component';
import { AuthService } from '../auth/auth.service';
import { User } from '../model/app.model';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  @ViewChild('userForm') userForm: NgForm
  dropdownList = []
  selectedItems = []
  User: User = { active: true } as any
  updateUser: boolean = false // true for submit and false for update
  constructor(private authService: AuthService,
    public dialogRef: MatDialogRef<UserManagementComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    if (data) {
      this.User = { ...data }
      this.updateUser = true
    }
  }

  ngOnInit(): void {
    const roles: any = this.authService.getRoles();
    this.dropdownList = roles;
  }
  onSubmit(form: NgForm) {
    if (!form.invalid) {
      if (this.updateUser) {
        this.authService.onUserListUpdate(this.User)
      } else {
        this.authService.newUser(this.User)
      }
      this.dialogRef.close()
    }
  }
  onClose() {
    this.dialogRef.close()
  }
}
