const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
    title: { type: String, required: true },
    country: { type: String, required: true },
    lang: { type: String, required: true },
    news: { type: String, required: true }
});

module.exports = mongoose.model('Log', postSchema);
