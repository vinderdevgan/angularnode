const jwt = require("jsonwebtoken");
const { config, verify } = require('./../config')

const auth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    // const decodedToken = jwt.verify(token, config.secretKey);
    const decodedToken = verify(token, config.secretKey);
    req.userData = { _id: decodedToken._id, username: decodedToken.username, email: decodedToken.email };
    next();
  } catch (error) {
    res.status(401).json({
      message: "Auth failed!",
      token: null,
      expiresIn: null
    });
  }
}
module.exports = { auth }
