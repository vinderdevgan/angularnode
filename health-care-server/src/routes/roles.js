const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { config, hash, compare, token } = require('./../config')
const { auth } = require("../middleware/check-auth");
const Role = require("../models/role");

const router = express.Router();

router.get("/", (req, res, next) => {
  Role.find()
  .then(documents => {
    res.status(200).json({
      roles: documents
    });
  });
});

router.post("/", async (req, res, next) => {
  const role = new Role({
    _id: null,
    role: req.body.role
  })
  role.save()
    .then(result => {
      let role = {
        _id: result._id,
        role: result.role
      }

      res.status(200).json({
        message: 'Created',
        role: role
      })
    })
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    })
})

module.exports = router;

