const mongoose = require("mongoose");

const testSchema = mongoose.Schema({
  _id: { type: String, required: true },
  name: { type: String, required: true },
  code: { type: String, required: true },
  group: { type: String, required: true },
  tubeType: { type: String, required: true },
});

testSchema.index({ code: 1, group: 1 }, { unique: true });
module.exports = mongoose.model("Test", testSchema);
