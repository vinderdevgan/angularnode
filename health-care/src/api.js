// const host = '13.59.117.41';
const host = '127.0.0.1';
const port = 3001;

export default function api(midUrl) {
    return `http://${host}:${port}/${midUrl}`
}