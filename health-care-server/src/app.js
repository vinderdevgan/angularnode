const express = require('express');
const bodyParser = require('body-parser');
const mangoose = require('mongoose');
const userRoutes = require('./routes/users')
const roleRoutes = require('./routes/roles')
const requisitionsRoutes = require('./routes/requisitions')
const testsRoutes = require('./routes/tests')
const pdfRoutes = require('./routes/pdf')
const { connectDB } = require('./config.js')
const app = express();
// mongodb://meandb:<password>@cluster0-shard-00-00-qrobu.mongodb.net:27017,cluster0-shard-00-01-qrobu.mongodb.net:27017,cluster0-shard-00-02-qrobu.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority
mongodb://meandb:<password>@cluster0-shard-00-00-qrobu.mongodb.net:27017,cluster0-shard-00-01-qrobu.mongodb.net:27017,cluster0-shard-00-02-qrobu.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority
// mangoose.connect(`mongodb+srv://meandb:${db.password}@cluster0-qrobu.mongodb.net/${db.name}?retryWrites=true`, { useNewUrlParser: true, useCreateIndex: true })

mangoose.connect(connectDB, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false })
  .then(() => {
    console.log('connected to database!');
  })
  .catch((e) => {
    console.log('connection failed' + e);
  });

app.use(express.static('./public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});
app.use("/api/role", roleRoutes);
app.use("/api/user", userRoutes);
app.use("/api/requisition", requisitionsRoutes);
app.use("/api/test", testsRoutes);
app.use("/api/pdf", pdfRoutes);



module.exports = app;
