const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { config, hash, compare, token } = require('./../config')
const { auth } = require("../middleware/check-auth");
const Patient = require("../models/patients");

const router = express.Router();

router.post("/", async (req, res, next) => {
  const user = new Patient({
    _id: null,
    name: req.body.name,
    role: req.body.role
  })
  user.save()
    .then(result => {
      let patient = {
        _id: result._id,
        role: result.role,
        name: result.name
      }

      res.status(200).json({
        message: 'Yeah!',
        patient: patient
      })
    })
    .catch(error => {
      res.status(500).json({
        message: 'user exist'
      })
    })
})

router.get("/", (req, res, next) => {
  Patient.find()
    .then(documents => {
      res.status(200).json({
        message: 'server get post',
        posts: documents
      });
    });
});


module.exports = router;

