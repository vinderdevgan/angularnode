import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgForm } from '@angular/forms'
import { AuthService } from '../auth/auth.service';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import * as JsBarcode from 'JsBarcode';
import _ from 'lodash'
import { take, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import PHE from 'print-html-element'
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import api from '../../api'
import { MatCheckboxChange, MatCheckbox } from '@angular/material/checkbox';
@Component({
  selector: 'entry-form',
  templateUrl: './entry-form.component.html',
  styleUrls: ['./entry-form.component.css']
})
export class EntryFormComponent implements OnInit {
  public testMultiCtrl: FormControl = new FormControl();
  public testMultiFilterCtrl: FormControl = new FormControl();

  public filteredTestsMulti: any = new ReplaySubject<any>(1);
  protected _onDestroy = new Subject<void>();

  practiceRequisitionList: any = JSON.parse(localStorage.getItem("practiceRequisitionList")) === null ? [] : JSON.parse(localStorage.getItem("practiceRequisitionList"));
  testRequisitionList: any = JSON.parse(localStorage.getItem("testRequisitionList")) === null ? [] : JSON.parse(localStorage.getItem("testRequisitionList"));
  dataInputs: any = JSON.parse(localStorage.getItem("dataInputs")) === null ? {} : JSON.parse(localStorage.getItem("dataInputs"));
  dropdownList = [];
  isLoading = false;
  toShow = false;
  selectItem = [];
  requisitionList = [];
  requisitionDropdownSettings: IDropdownSettings = {
    singleSelection: true,
    idField: '_id',
    textField: '_id',
    itemsShowLimit: 8,
    allowSearchFilter: false,
    showSelectedItemsAtTop: false
  };
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: '_id',
    textField: 'codeAndName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 8,
    allowSearchFilter: true
  };
  mode: any = 'practice';
  initialState: any = {
    tests: []
  }
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'left';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  printOption = {
    barcode: false,
    requisition: false,
    label: false
  };
  labelsObject = {};
  requisitionObj = {};
  selectedPrintOptions = {
    requisition: false,
    barcode: false,
    label: false
  }

  constructor(private authService: AuthService, public snackBar: MatSnackBar) { }
  ngOnInit(): void {
    this.authService.getTestslist();
    this.authService.getRequisition();

    this.testMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTestsMulti();
      });

    this.authService.getRequisitionListListener()
      .subscribe((res: any) => {
        this.requisitionList = res
      })

    this.authService.getTestListListener()
      .subscribe((res: any) => {
        res.map((data, index) => {
          res[index].codeAndName = `${data.code}${' '}-${' '}${data.name}-${' '}${data.tubeType}`;
        })
        const sortedData = _.orderBy(res, [r => r.codeAndName.toLowerCase()], ['asce']);
        this.dropdownList = sortedData;
        this.filteredTestsMulti.next(this.dropdownList.slice());
      })
  }
  updateMode(e) {
    this.toShow = false
    this.selectItem = []
    this.mode = e;
    this.initialState = {
      tests: []
    }
    this.filteredTestsMulti.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.testMultiCtrl.patchValue([]);
      })
  }

  protected filterTestsMulti() {
    if (!this.dropdownList) {
      return;
    }
    // get the search keyword
    let search = this.testMultiFilterCtrl.value;
    if (!search) {
      this.filteredTestsMulti.next(this.dropdownList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredTestsMulti.next(
      this.dropdownList.filter(e => e.codeAndName.toLowerCase().indexOf(search) > -1)
    );
  }

  toggleSelectAll(selectAllValue: boolean) {
    this.filteredTestsMulti.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(val => {
        if (selectAllValue) {
          this.testMultiCtrl.patchValue(val);
        } else {
          this.testMultiCtrl.patchValue([]);
        }
      });
  }

  async generatePdf() {
    const clone = JSON.parse(JSON.stringify(this.initialState))
    if (!clone.tests) clone.tests = [];
    clone.tests.map((t, index) => {
      const data = this.dropdownList.filter((test) => test._id === t._id)[0]
      clone.tests[index] = data.code
    })
    const res = await this.authService.generatePdf(clone)
    return res
  }

  async generateRandomRequisition() {
    await this.authService.generateRandomPdf(this.mode).then(({ requisition, url }) => {
      if (this.mode === 'practice') {
        const alreadyExist = this.practiceRequisitionList.filter((req) => req._id === requisition._id)[0]
        if (!alreadyExist) {
          this.practiceRequisitionList.push(requisition);
          this.practiceRequisitionList = _.orderBy(this.practiceRequisitionList, [r => r.lastName], ['asce']);
          this.dataInputs[requisition.accession] = {
            accession: requisition.accession
          }
          localStorage.setItem('practiceRequisitionList', JSON.stringify(this.practiceRequisitionList));
          const dataInputs = JSON.parse(localStorage.getItem("dataInputs"))
          if (dataInputs === null) {
            localStorage.setItem('dataInputs', JSON.stringify({ [requisition.accession]: { accession: requisition.accession } }));
          } else if (!dataInputs[requisition.accession]) {
            localStorage.setItem('dataInputs', JSON.stringify({ ...dataInputs, [requisition.accession]: { accession: requisition.accession } }));
          }
        }
      }
      if (this.mode === 'test') {
        const alreadyExist = this.testRequisitionList.filter((req) => req._id === requisition._id)[0]
        if (!alreadyExist) {
          this.testRequisitionList.push(requisition);
          this.testRequisitionList = _.orderBy(this.testRequisitionList, [r => r.lastName], ['asce']);
          this.dataInputs[requisition.accession] = {
            accession: requisition.accession
          }
          localStorage.setItem('testRequisitionList', JSON.stringify(this.testRequisitionList));
          const dataInputs = JSON.parse(localStorage.getItem("dataInputs"))
          if (dataInputs === null) {
            localStorage.setItem('dataInputs', JSON.stringify({ [requisition.accession]: { accession: requisition.accession } }));
          } else if (!dataInputs[requisition.accession]) {
            localStorage.setItem('dataInputs', JSON.stringify({ ...dataInputs, [requisition.accession]: { accession: requisition.accession } }));
          }
        }
      }
      const downloadElement: any = document.getElementsByClassName("donwload")[0];

      downloadElement.href = `${api(url)}`
      downloadElement.click()
    })
  }

  initializeForm(form: any) {
    if (form.accession === this.initialState.accession) {
      this.initialState = {
        tests: []
      }
      this.filteredTestsMulti.pipe(take(1), takeUntil(this._onDestroy))
        .subscribe(() => {
          this.testMultiCtrl.patchValue([]);
        })
      return
    }
    if (this.dataInputs[form.accession]) {
      const testList = []
      this.dataInputs[form.accession].tests && this.dataInputs[form.accession].tests.map((test) => {
        testList.push(this.dropdownList.filter((t) => t._id === test._id)[0])
      })
      this.initialState = this.dataInputs[form.accession]
      this.testMultiCtrl.patchValue(testList);
    } else {
      this.initialState = { accession: form.accession }
    }
  }

  async print() {
    if (!this.initialState.accession) {
      this.openSnackbar('Fill Requsition to Print')
    } else {
      const pdfResponse = await this.generatePdf()
      const downloadElement: any = document.getElementsByClassName("donwload")[0];
      downloadElement.href = `${api(pdfResponse.url)}`
      downloadElement.click()
    }
  }
  compare() {
    this.toShow = !this.toShow
  }


  async multiplePdf() {
    this.isLoading = true
    const res = await this.authService.generatemultipleRandomPdf()
    this.isLoading = false
    var link = document.createElement('a');
    link.setAttribute('download', null);
    link.setAttribute('target', '_blank');
    link.style.display = 'none';
    document.body.appendChild(link);
    link.setAttribute('href', `${api(res.url)}`);
    link.click();
    document.body.removeChild(link);
  }

  isCorrect(field) {
    if (this.initialState.accession) {
      const requisition = this.practiceRequisitionList.filter((requisition) => requisition.accession === this.initialState.accession)[0]
      if (requisition) {
        if (field === 'dob' || field === 'collectionDate') {
          const a = moment(requisition[field]).format("DD/MM/YYYY");
          const b = moment(this.initialState[field]).format("DD/MM/YYYY")
          return a === b
        }
        if (field === 'tests') {
          const listToCompate = []
          this.testMultiCtrl.value && this.testMultiCtrl.value.map((test: any) => {
            listToCompate.push(test.code)
          })
          return JSON.stringify(_.orderBy(requisition[field], [r => r], ['asce'])) === JSON.stringify(_.orderBy(listToCompate, [r => r], ['asce']))
        }
        return requisition[field] === this.initialState[field]
      }
    }
  }

  openSnackbar(message) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this.snackBar.open(message, undefined, config)
  }

  saveToList(form: NgForm) {

    if (!this.initialState.phn) {
      this.openSnackbar('Select Requsition to Save')
    }
    if (!form.invalid) {
      let tests = []
      if (this.testMultiCtrl.value) {
        tests = this.testMultiCtrl.value
      }
      this.dataInputs[this.initialState.accession].tests = tests
      let mform = JSON.parse(JSON.stringify(this.dataInputs));
      localStorage.setItem('dataInputs', JSON.stringify(mform));
      this.openSnackbar('Saved to List')
    }
  }

  onListSelect(event: MatCheckboxChange) {
    if (event.checked) {
      this.selectItem.push(event.source.value)
    } else {
      this.selectItem = this.selectItem.filter(item => item != event.source.value)
    }

  }
  onPrint() {
    this.selectItem.map((requisitionId) => {
      let testLabel = []
      const requisition = this.dataInputs[requisitionId]
      if (!requisition.tests) requisition.tests = []
      requisition.tests && requisition.tests.map((test) => {
        testLabel.push(this.dropdownList.filter((t) => t._id === test._id)[0]);
        const id = '#x'.concat(requisitionId)
        setTimeout(() => {
          if (document.getElementById('x'.concat(requisitionId))) {
            JsBarcode(String(id), String(this.dataInputs[requisitionId].accession), {
              format: "code128",
              height: 30,
              width: 1.5,
              background: 'rgba(255, 69, 0, 0)',
              displayValue: false
            })
          }
        }, 0)
      })
      this.labelsObject[requisition.accession] = testLabel

    })
    setTimeout(() => {
      let height = 0
      const element: any = document.getElementById("page");
      for (let i = 0; i < element.children.length; i++) {
        const elementHeight = element.children[i].offsetHeight;
        height = height + elementHeight
        if (height > 842) {
          height = elementHeight;
          element.children[i].style.pageBreakBefore = "always"
        } else {
          element.children[i].style.pageBreakBefore = "unset"
        }
      }
      PHE.printElement(document.getElementById('page'))
    }, 0)
  }

  onSelectOptions(event) {
    if (this.selectedPrintOptions[event.source.value]) {
      this.selectedPrintOptions[event.source.value] = false
    } else {
      this.selectedPrintOptions[event.source.value] = true
    }
  }


}