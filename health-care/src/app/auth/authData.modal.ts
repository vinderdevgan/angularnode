export interface AuthLogin {
    username: String,
    password: String
}
export interface AuthUser {
    _id: null | String,
    username: String,
    email: String,
    name: String
    password: String,
    status: String,
    groups: String
}