const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { config, hash, compare, token } = require('../config')
const { auth } = require("../middleware/check-auth");
const Test = require("../models/tests");

const router = express.Router();

router.post("/", async (req, res, next) => {
  const test = new Test({
    _id: null,
    name: req.body.name,
    code: req.body.code,
    group: req.body.group
  })

  test.save()
    .then(result => {
      res.status(200).json({
        message: 'Yeah!'
      })
    })
    .catch(error => {
      res.status(500).json({
        message: error.message
      })
    })
})

router.get("/", (req, res, next) => {
  Test.find()
    .then(documents => {
      res.status(200).json({
        tests: documents
      });
    });
});


module.exports = router;

