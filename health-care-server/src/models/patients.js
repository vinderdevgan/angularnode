const mongoose = require("mongoose");
const Schema = mongoose.Schema
const uniqueValidator = require("mongoose-unique-validator");

const patientSchema = mongoose.Schema({
  _id: { type: String, required: true },
  name: { type: String, required: true },
  role: { type: Schema.Types.ObjectId, ref: 'Role' }
});

patientSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Patient", patientSchema);
