const { generateObject } = require('./pdfObjectGenearator');
const _ = require('lodash');
const pdftk = require('node-pdftk');

async function generatePdf(val) {
  let res;
  let date = Date.now();
  await pdftk
    .input('./reqform16_fillable.pdf')
    .fillForm(generateObject(val))
    .flatten()
    .output(`./public/${date}.pdf`)
    .then(() => {
      res = { message: 'success', url: `${date}.pdf` }
    })
    .catch(err => {
      res = { message: err }
    });
  return res
};

module.exports = { generatePdf };