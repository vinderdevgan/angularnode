const moment = require('moment');

const initialState = {
    top_phn: '',
    top_appt_datetime: '',
    access_number: '',
    top_male: '',
    top_female: '',
    top_lastname: '',
    top_firstname: '',
    top_initial: '.',
    top_dob_dd: '',
    top_dob_mm: '',
    top_dob_yyyy: '',
    top_address: '',
    top_city: '',
    top_province: '',
    top_postalcode: '',
    chart_patient_phone_lab: '',
    patient_phone_number: '',
    ordering_physician_practitioner: '',
    physician_code_2: '',
    ordering_address_location: '',
    report_location_code: '',
    report_address_if_different: '',

    IA: 'No',
    IP: 'No',
    OP: 'No',
    AP: 'No',
    HC: 'No',
    ST: 'No',
    EN: 'No',

    DD_2: '',
    MM_2: '',
    YYYY_2: '',
    col_location: '',
    collector: '',

    CBC: 'No',
    CBCD: 'No',
    HB: 'No',
    HCT: 'No',

    PLT: 'No',
    WBC: 'No',
    RETIC: 'No',
    PC: 'No',
    MAL: 'No',
    PT: 'No',
    PTT: 'No',
    FIB: 'No',
    QDDIM: 'No',
    THAL: 'No',
    SHBS: 'No',
    FER: 'No',
    B12: 'No',

    UMA: 'No',
    UTPCR: 'No',
    PREG: 'No',
    BJ: 'No',

    UNA: 'No',
    UK: 'No',
    UCL: 'No',

    UTP: 'No',
    UCRE: 'No',
    CRCL: 'No',


    UMET: 'No',
    UCOR: 'No',
    UALB: 'No',


    SFGLU: 'No',
    SFTP: 'No',
    SFCT: 'No',
    FLGLU: 'No',
    FLTP: 'No',
    FLCT: 'No',
    FLCRY: 'No',

    HBA1C: 'No',
    UALBR: 'No',

    GLUCF: 'No',
    GLUCP: 'No',
    GLUCR: 'No',
    GTT2: 'No',
    NA: 'No',
    K: 'No',
    CL: 'No',
    CO2: 'No',
    CREA: 'No',
    CA: 'No',
    LD: 'No',
    P04: 'No',
    MG: 'No',
    TP: 'No',
    ALB: 'No',
    ALP: 'No',
    ALT: 'No',
    UA: 'No',
    TBIL: 'No',
    CBIL: 'No',
    NBIL: 'No',
    LPS: 'No',
    GGT: 'No',
    CK: 'No',
    TROP: 'No',
    C3: 'No',
    C4: 'No',
    IGQ: 'No',
    IGE: 'No',
    SPE: 'No',
    LDL: 'No',
    HDL: 'No',
    TRIG: 'No',
    CHOL: 'No',
    HBP: 'No',
    UAMP: 'No',
    UMAI: 'No',
    CHOL_DIAB: 'No',


    HB: 'No',
    UMA: 'No',
    GDS: 'No',
    GTTPR: 'No',
    UALBR: 'No',

    HIV: 'No',
    TRPR: 'No',
    UMA: 'No',

    HAVM: 'No',
    HAVG: 'No',
    HSAG: 'No',
    HSAB: 'No',
    HCVAB: 'No',
    HCV: 'No',

    ANA: 'No',
    ATTG: 'No',
    RA: 'No',
    ASOT: 'No',
    SYPH: 'No',
    RUBG: 'No',
    MONOS: 'No',
    EBM: 'No',
    TPO: 'No',

    CORA: 'No',
    CORP: 'No',
    DHEAS: 'No',
    E2: 'No',
    FSH: 'No',
    LH: 'No',
    PROG: 'No',
    PRL: 'No',
    PTH: 'No',
    HCG: 'No',
    TSHBO: 'No',
    TSHB: 'No',
    TESTA: 'No',
    TESTP: 'No',

    OB: 'No',
    PSEA: 'No',
    HIVAB: 'No',
    XPSA: 'No',
    ECG: 'No',

    UCHL: 'No',
    UGC: 'No',
    SCTGC: 'No',

    ACET: 'No',
    SAL: 'No',
    ETOH: 'No',
    BARB: 'No',
    BENZ: 'No',
    TCA: 'No',

    UAMP: 'No',
    UBARB: 'No',
    UBENZ: 'No',
    UCAN: 'No',
    UCOC: 'No',
    UMETM: 'No',
    UOP: 'No',
    UOXY: 'No',
    UTCA: 'No',

    CARB: 'No',
    CYCLO: 'No',
    CYCL2: 'No',
    DIG: 'No',
    LI: 'No',
    PHB: 'No',
    PTN: 'No',
    SIRO: 'No',
    TAC: 'No',
    VA: 'No'
}

function generateObject(req) {
    const pdfFields = { ...initialState }
    const { phn = '', lastName = '', firstName = '', sex = '', collect = '', postalCode = '', physician = '', physicianCode = '', tests = '', accession = '', collectionLocation = '' } = req;
    pdfFields.top_phn = phn;
    pdfFields.top_lastname = lastName;
    pdfFields.top_firstname = firstName;
    pdfFields.top_postalcode = postalCode;
    pdfFields.ordering_physician_practitioner = physician;
    pdfFields.physician_code_2 = physicianCode;
    pdfFields.access_number = accession;
    pdfFields.col_location = collectionLocation;
    pdfFields.top_dob_dd = req.dob ? moment(req.dob, 'YYYY-MM-DD').format('DD') : ''
    pdfFields.top_dob_mm = req.dob ? moment(req.dob, 'YYYY-MM-DD').format('MM') : ''
    pdfFields.top_dob_yyyy = req.dob ? moment(req.dob, 'YYYY-MM-DD').format('YYYY') : ''
    pdfFields.DD_2 = req.collectionDate ? moment(req.collectionDate, 'YYYY-MM-DD').format('DD') : ''
    pdfFields.MM_2 = req.collectionDate ? moment(req.collectionDate, 'YYYY-MM-DD').format('MM') : ''
    pdfFields.YYYY_2 = req.collectionDate ? moment(req.collectionDate, 'YYYY-MM-DD').format('YYYY') : ''

    if (sex === 'M') {
        pdfFields.top_male = 'Yes'
    } else if (sex === 'F') {
        pdfFields.top_female = 'Yes'
    }

    tests.map((test) => {
        pdfFields[test] = 'Yes'
    })
    return pdfFields
}

module.exports = { generateObject }