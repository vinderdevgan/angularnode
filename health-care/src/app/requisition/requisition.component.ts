import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgForm } from '@angular/forms'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { AuthService } from '../auth/auth.service';
import { take, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import * as JsBarcode from 'JsBarcode';
import * as moment from 'moment';
import { ReplaySubject, Subject } from 'rxjs';
import _ from 'lodash';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import api from '../../api'
@Component({
  selector: 'RequisitionComponent',
  templateUrl: './requisition.component.html',
  styleUrls: ['./requisition.component.css']
})
export class RequisitionComponent implements OnInit {
  public testMultiCtrl: FormControl = new FormControl();
  public testMultiFilterCtrl: FormControl = new FormControl();
  public requisitionCtrl: FormControl = new FormControl();
  public requisitionFilterCtrl: FormControl = new FormControl();

  public filteredTestsMulti: any = new ReplaySubject<any>(1);
  public filteredRequestions: any = new ReplaySubject<any>(1);

  dropdownList = [];
  requisitionList = [];
  selectedRequisition = null;
  requisition = null;
  showBarCode = false
  requisitionDropdownSettings: IDropdownSettings = {
    singleSelection: true,
    idField: '_id',
    textField: 'phnAndLastName',
    itemsShowLimit: 10,
    allowSearchFilter: false,
    showSelectedItemsAtTop: false
  };
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: '_id',
    textField: 'codeAndName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 10,
    allowSearchFilter: true,

  };
  initialState: any = {
    tests: [],
    mode: 'practice'
  };
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'left';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  protected _onDestroy = new Subject<void>();

  constructor(private authService: AuthService, public snackBar: MatSnackBar) { }
  ngOnInit(): void {
    this.authService.getTestslist();
    this.authService.getRequisition();

    this.testMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTestsMulti();
      });

    this.requisitionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterRequestion();
      });

    this.authService.getRequisitionListListener()
      .subscribe((res: any) => {
        const sortedData = _.orderBy(res, [r => r.lastName], ['asce']);
        this.requisitionList = sortedData;
        this.filteredRequestions.next(this.requisitionList)
        res.map((data, index) => {
          res[index].phnAndLastName = `${data.phn}${' '}-${' '}${data.lastName}`;
        })
      })

    this.authService.getTestListListener()
      .subscribe((res: any) => {
        res.map((data, index) => {
          res[index].codeAndName = `${data.code}${' '}-${' '}${data.name}-${' '}${data.tubeType}`;
        })
        const sortedData = _.orderBy(res, [r => r.codeAndName.toLowerCase()], ['asce']);
        this.dropdownList = sortedData;
        this.filteredTestsMulti.next(this.dropdownList.slice());
      })

  }

  openSnackbar(message) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this.snackBar.open(message, undefined, config)
  }

  protected filterTestsMulti() {
    if (!this.dropdownList) {
      return;
    }
    // get the search keyword
    let search = this.testMultiFilterCtrl.value;
    if (!search) {
      this.filteredTestsMulti.next(this.dropdownList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredTestsMulti.next(
      this.dropdownList.filter(e => e.codeAndName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterRequestion() {
    if (!this.requisitionList) {
      return;
    }
    // get the search keyword
    let search = this.requisitionFilterCtrl.value;
    if (!search) {
      this.filteredRequestions.next(this.requisitionList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.filteredRequestions.next(
      this.requisitionList.filter(e => e.phnAndLastName.toLowerCase().indexOf(search) > -1)
    );
  }

  toggleSelectAll(selectAllValue: boolean) {
    this.filteredTestsMulti.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(val => {
        if (selectAllValue) {
          this.testMultiCtrl.patchValue(val);
        } else {
          this.testMultiCtrl.patchValue([]);
        }
      });
  }

  onRequisitionSelect(e: any) {
    this.requisition = JSON.parse(JSON.stringify(this.requisitionList.filter((req: any) => req._id === e._id)[0]));
    this.requisition.tests && this.requisition.tests.map((test, index) => {
      this.requisition.tests.splice(index, 1, this.dropdownList.filter(t => t._id == test)[0])
    })
    this.requisition.dob = moment(this.requisition.dob).format("YYYY-MM-DD");
    this.requisition.collectionDate = moment(this.requisition.collectionDate).format("YYYY-MM-DD");
    this.initialState = this.requisition
    this.testMultiCtrl.setValue(this.requisition.tests)
    this.filteredRequestions.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.requisitionCtrl.patchValue(this.requisitionList.filter((req: any) => req._id === e._id)[0]);
      })
    JsBarcode("#code128", String(this.requisition.phn), {
      format: "code128",
      height: 30,
      width: 1.5,
      background: 'rgba(255, 69, 0, 0)',
      displayValue: false
    });
    this.showBarCode = true
  }

  onRequisitionDeselect() {
    this.selectedRequisition = null;
    this.requisition = null
    this.initialState = {};
    this.showBarCode = false;
    this.filteredTestsMulti.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.testMultiCtrl.patchValue([]);
      })
    this.filteredRequestions.pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.requisitionCtrl.patchValue([]);
      })
  }

  async generatePdf() {
    const clone = JSON.parse(JSON.stringify(this.initialState))
    if (!clone.tests) clone.tests = [];
    clone.tests.map((t, index) => {
      const data = this.dropdownList.filter((test) => test._id === t._id)[0]
      clone.tests[index] = data.code
    })

    const pdf = await this.authService.generatePdf(clone)
    const downloadElement: any = document.getElementsByClassName("donwload")[0];
    downloadElement.href = `${api(pdf.url)}`
    downloadElement.click()
  }

  async onSubmit(form: NgForm) {
    if (!form.invalid) {
      if (this.initialState.phn) {
        form.value._id = this.initialState._id;
        form.value.phn = this.initialState.phn;
        form.value.accession = this.initialState.accession;
      }
      if (!form.value.hidden) {
        form.value.hidden = false
      }
      let mform = JSON.parse(JSON.stringify(form.value));
      let tests = [];
      mform.tests && mform.tests.forEach((ee) => { tests.push(ee._id) })
      mform.tests = this.testMultiCtrl.value
      let res
      try {
        res = await this.authService.saveRequisition(mform)
      } catch (error) {
      }
      if (res) {
        if (res.message === "Created") {
          this.openSnackbar('Created')
          this.selectedRequisition = [{ _id: res.data._id, phnAndLastName: `${res.data.phn}${' '}-${' '}${res.data.lastName}` }]
          this.onRequisitionSelect({ _id: res.data._id, phnAndLastName: `${res.data.phn}${' '}-${' '}${res.data.lastName}` })
          this.filteredRequestions.pipe(take(1), takeUntil(this._onDestroy))
            .subscribe(() => {
              this.requisitionCtrl.patchValue(this.requisitionList.filter((e) => e._id === res.data._id)[0]);
            })
        } else {
          this.filteredRequestions.pipe(take(1), takeUntil(this._onDestroy))
            .subscribe(() => {
              this.requisitionCtrl.patchValue(this.requisitionList.filter((e) => e._id === form.value._id)[0]);
            })
          this.openSnackbar('Updated')
        }
      } else {
        this.openSnackbar('Error Creating Requisition')
      }
    }
  }


  async deleteRequisition() {
    await this.authService.removeRequisition(this.requisitionCtrl.value._id)
    this.onRequisitionDeselect()
    this.openSnackbar('Deleted')
  }
}
